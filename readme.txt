In the repository you will find:
- The PDF with the spoken sentences and a description of the work.
- The Protocol e details about the wav files.
- The WAV files:
  They are named according to the following pattern: STANCE_(M/F Number 01-20)_(Number 00-23)_(Number 1-5).wav
	STANCE: 		one of the 8 categories of interpersonal stances.
	(M/F Number 01-20) :	M= Male F= Female the number identifies which person recorded the voice from person 01 to person 20. 
	(Number 00-23)	   : 	Number of the sentence described in the PDF. Each person has 24 sentences spoken into the 8 categories.
	(Number 1-5)	   :	The last number can be ignored. It does not provide any information.
	Example 1:  Aggressive_f10_05_1.wav  : Stance: Aggressive, Female 1, sentence 5 "Follow me, I know the way, let�s turn right here and go straight".
	Example 2:  Withdraw_m10_00_1.wav    : Stance: Withdraw,   Male  10, sentence 0 "Go ahead, I know that you can deal with it and overcome this problem!".
